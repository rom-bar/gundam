package main

import (
	"fmt"
	"github.com/casbin/casbin/v2"
	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

var jwtKey []byte

var db *gorm.DB

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	secKey := os.Getenv("SECRET_KEY")
	if secKey == "" {
		log.Fatal("Secret Key is empty, quit application")
	}
	jwtKey = []byte(secKey)

	dbSelection := os.Getenv("DB_SELECT")
	dbParam := ""

	if dbSelection == "postgres" {
		//db, err := gorm.Open("postgres", "host=myhost port=myport user=gorm dbname=gorm password=mypassword")
		host := os.Getenv("POSTGRES_HOST")
		port := os.Getenv("POSTGRES_PORT")
		user := os.Getenv("POSTGRES_USER")
		dbName := os.Getenv("POSTGRES_DB_NAME")
		password := os.Getenv("POSTGRES_PASSWORD")
		dbParam = fmt.Sprintf("host=%v port=%v user=%v dbname=%v password=%v", host, port, user, dbName, password)
		log.Println("Using postgre with param ", dbParam)
	}

	var dbInit *gorm.DB
	dbInit, err = initDb(dbSelection, dbParam)
	if err != nil {
		log.Fatal("Init database failed, quit")
	}
	db = dbInit
	defer func() {
		err = db.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.Secure())
	e.Use(middleware.CORS())
	enforcer, err := casbin.NewEnforcer("./rbac_model.conf", "./rbac_policy.csv")
	if err != nil {
		log.Fatal("Failed to enforce Role, Quit Application ", err)
	}
	config := localConfig{
		Skipper:  middleware.DefaultSkipper,
		Enforcer: enforcer,
	}
	e.Use(checkPermission(config))
	//all access
	e.POST("/login", login)
	//Admin only access
	e.POST("/create", createUser)
	e.POST("/delete", deleteUser)
	//Member access
	e.POST("/read", readUser)
	e.POST("/update", updateUser)

	e.Logger.Fatal(e.Start(":1323"))
}

func checkPermission(config localConfig) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			if pass, err := config.CheckPermission(c); err == nil && pass {
				return next(c)
			} else if err != nil {
				return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
			}
			return echo.ErrForbidden
		}
	}
}

func (a *localConfig) CheckPermission(c echo.Context) (bool, error) {
	user, err := getClaims(c)
	if err != nil {
		return false, err
	}
	if user.Role == "" {
		user.Role = "anonymous"
	}
	method := c.Request().Method
	path := c.Request().URL.Path
	c.Set("user", user)
	return a.Enforcer.Enforce(user.Role, path, method)
}

func getClaims(c echo.Context) (myClaims, error) {
	_claims := myClaims{}

	tknStr, err := c.Cookie("token")

	if err != nil {
		return _claims, nil
	}

	if tknStr.Value == "" {
		return _claims, nil
	}

	tkn, err := jwt.ParseWithClaims(tknStr.Value, &_claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			return _claims, echo.ErrUnauthorized
		}
		log.Println("error", err)
		return _claims, echo.ErrForbidden
	}
	if !tkn.Valid {
		return _claims, echo.ErrUnauthorized
	}
	return _claims, nil
}

func createUser(c echo.Context) error {
	username := c.FormValue("username")
	if username == "" {
		return c.String(http.StatusBadRequest, "USERNAME_MISSING")
	}

	password := c.FormValue("password")
	if password == "" {
		return c.String(http.StatusBadRequest, "PASSWORD_MISSING")
	}

	role := c.FormValue("role")
	if role == "" {
		return c.String(http.StatusBadRequest, "ROLE_MISSING")
	}

	listAddress := c.FormValue("listAddress")
	if listAddress == "" {
		return c.String(http.StatusBadRequest, "LIST_ADDRESS_MISSING")
	}

	err := saveUserDB(username, password, role, strings.Split(listAddress, "|"))

	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}

	return c.String(http.StatusOK, "success")
}
func deleteUser(c echo.Context) error {
	userID := c.FormValue("id")
	if userID == "" {
		return c.String(http.StatusBadRequest, "ID_MISSING")
	}
	err := deleteUserDB(userID)

	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}

	return c.String(http.StatusOK, "success")
}

func updateUser(c echo.Context) error {
	userID := c.FormValue("id")
	if userID == "" {
		return c.String(http.StatusBadRequest, "ID_MISSING")
	}

	currentUser := c.Get("user").(*user)

	if currentUser.Role == "anonymous" {
		return echo.ErrForbidden
	}
	if currentUser.Role == "member" && strconv.Itoa(int(currentUser.ID)) != userID {
		return echo.ErrForbidden
	}

	username := c.FormValue("username")
	password := c.FormValue("password")
	role := c.FormValue("role")
	listAddress := c.FormValue("listAddress")

	err := updateUserDB(userID, username, password, role, strings.Split(listAddress, "|"))

	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	return c.String(http.StatusOK, "success")
}

func readUser(c echo.Context) error {
	userID := c.FormValue("id")
	if userID == "" {
		return c.String(http.StatusBadRequest, "ID_MISSING")
	}

	currentUser := c.Get("user").(*user)

	if currentUser.Role == "anonymous" {
		return echo.ErrForbidden
	}
	if currentUser.Role == "member" && strconv.Itoa(int(currentUser.ID)) != userID {
		return echo.ErrForbidden
	}
	realUser, err := getUserDB(userID)

	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	return c.JSON(http.StatusOK, &realUser)
}

func login(c echo.Context) error {
	username := c.FormValue("username")
	password := c.FormValue("password")

	res, currUser := checkUser(username, password)

	if res == false {
		return echo.ErrUnauthorized
	}

	expirationTime := time.Now().Add(5 * time.Hour)
	claims := &myClaims{
		ID:   currUser.ID,
		Role: currUser.Role,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		return echo.ErrInternalServerError
	}

	cookie := new(http.Cookie)
	cookie.Name = "token"
	cookie.Value = tokenString
	cookie.Expires = expirationTime
	c.SetCookie(cookie)
	return c.JSON(http.StatusOK, "success")
}
