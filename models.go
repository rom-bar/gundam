package main

import (
	"github.com/casbin/casbin/v2"
	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4/middleware"
)

type localConfig struct {
	Skipper  middleware.Skipper
	Enforcer *casbin.Enforcer
}

type myClaims struct {
	ID   uint
	Role string
	jwt.StandardClaims
}

type user struct {
	gorm.Model
	Name        string    `gorm:"unique;not null"`
	Password    string    `gorm:"not null"`
	Role        string    `gorm:"not null"`
	ListAddress []address `gorm:"foreignkey:UserRefer"`
}

type address struct {
	gorm.Model
	Content   string `gorm:"not null"`
	UserRefer uint   `gorm:"not null"`
}
