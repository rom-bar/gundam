package main

import (
	"errors"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"golang.org/x/crypto/bcrypt"
	"log"
	"strconv"
)

const DefBcryptCost = 13

func initDb(dbSelect, dbParam string) (*gorm.DB, error) {
	var db *gorm.DB
	var err error
	if dbSelect == "postgres" {
		db, err = gorm.Open("postgres", dbParam)
	} else if dbSelect == "inmemory" {
		db, err = gorm.Open("sqlite3", ":memory:")
		//db, err = gorm.Open("sqlite3", ":memory:")
	} else {
		log.Fatal("No database selected, quit application")
	}

	if err != nil {
		log.Fatal("Failed to init Database, just quit application : ", err)
	}

	db.LogMode(true)

	err = db.AutoMigrate(&user{}).Error

	if err != nil {
		log.Fatal("Failed to create user table, quit application")
	}

	err = db.AutoMigrate(&address{}).Error

	if err != nil {
		log.Fatal("Failed to create address table, quit application")
	}

	var adminUser user

	err = db.Where("name = ?", "Admin").Find(&adminUser).Error

	if err != nil || adminUser.Name == "" {
		var hashedPassword []byte
		log.Println("an error occured when find admin", err)
		//Admin maybe not found or not created so create it
		password := []byte("PetroseaAdmin")

		hashedPassword, err = bcrypt.GenerateFromPassword(password, DefBcryptCost)
		if err != nil {
			log.Fatal("Failed to generate Password Admin, quit application", err)
		}
		err = db.Create(&user{
			Name:     "Admin",
			Password: string(hashedPassword),
			Role:     "admin",
		}).Error
		if err != nil {
			log.Fatal("Failed to create Admin user, quit application ", err)
		}
	}

	return db, err
}

func saveUserDB(name, password, role string, listAddress []string) error {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), DefBcryptCost)
	if err != nil {
		return errors.New("HASH_PASS_FAILED")
	}

	var currentListAddress []address

	if len(listAddress) > 0 {
		for _, d := range listAddress {
			_address := address{
				Content: d,
			}
			currentListAddress = append(currentListAddress, _address)
		}
	}
	currentUser := user{
		Name:        name,
		Password:    string(hashedPassword),
		Role:        role,
		ListAddress: currentListAddress,
	}
	err = db.Create(&currentUser).Error
	if err != nil {
		return err
	}

	return nil
}

func updateUserDB(id, name, password, role string, listAddress []string) error {
	currentID, err := strconv.ParseUint(id, 10, 64)

	if err != nil {
		return errors.New("ID_MISSING")
	}
	var currentUser user
	err = db.First(&currentUser, "id = ?", currentID).Error
	if err != nil {
		return err
	}

	if name == "" {
		name = currentUser.Name
	}

	if password == "" {
		password = currentUser.Password
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), DefBcryptCost)
	if err != nil {
		return errors.New("HASH_PASS_FAILED")
	}

	if role == "" {
		role = currentUser.Role
	}
	if len(currentUser.ListAddress) > 0 {
		err = db.Where("user_refer = ?", id).Delete(address{}).Error
		if err != nil {
			return err
		}
	}
	var currentListAddress []address

	if len(listAddress) < 1 {
		currentListAddress = currentUser.ListAddress
	} else {
		for _, d := range listAddress {
			_address := address{
				Content: d,
			}
			currentListAddress = append(currentListAddress, _address)
		}
	}
	currentUser = user{
		Name:        name,
		Password:    string(hashedPassword),
		Role:        role,
		ListAddress: currentListAddress,
	}

	return db.Update(&currentUser).Error
}

func deleteUserDB(id string) error {
	currentID, err := strconv.ParseUint(id, 10, 64)

	if err != nil {
		return errors.New("ID_MISSING")
	}
	tx := db.Begin()

	err = tx.Where("user_refer = ?", id).Delete(address{}).Error
	if err != nil {
		tx.Rollback()
		return err
	}
	err = tx.Delete(&user{
		Model: gorm.Model{ID: uint(currentID)},
	}).Error

	if err != nil {
		tx.Rollback()
	}
	tx.Commit()
	return err
}

func checkUser(username, password string) (bool, user) {
	var currentUser user
	err := db.Set("gorm:auto_preload", true).First(&currentUser, "name = ?", username).Error
	if err != nil {
		return false, currentUser
	}
	err = bcrypt.CompareHashAndPassword([]byte(currentUser.Password), []byte(password))
	if err != nil {
		return false, currentUser
	}
	return true, currentUser
}

func getUserDB(id string) (user, error) {
	var currentUser user
	currentID, err := strconv.ParseUint(id, 10, 64)

	if err != nil {
		return currentUser, errors.New("ID_MISSING")
	}
	err = db.Set("gorm:auto_preload", true).First(&currentUser, "id = ?", currentID).Error
	if err != nil {
		return currentUser, err
	}

	currentUser.Password = "" //clear it so we don't show hashed password
	return currentUser, nil
}
