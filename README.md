Selamat datang di Repository Online Asessment Petrosea


Berikut cara penggunaan aplikasi pada repo ini.
1. Setup file .env , buat file .env baru yang akan digunakan untuk setting aplikasi
2. Contoh .env bisa di lihat pada .example.env
3. Untuk inmemory DB menggunakan Sqlite3 , masukkan parameter "inmemory" pada variable DB_SELECT di .env
4. Untuk postgresql DB masukkan "postgres" pada variable DB_SELECT, lalu masukkan input pada variable POSTGRES_*
5. Tentukan secret key yang akan digunakan untuk JWT pada variable SECRET_KEY

Default login admin adalah
username = "Admin"
password = "PetroseaAdmin"

Untuk Auth menggunakan JWT yang di set pada cookie.
Sehingga untuk setiap request selain dari login membutuhkan cookie yang berisi token JWT.

Maka dari itu aplikasi ini membutuhkan SecretKey yang akan di gunakan untuk Sign token tersebut.

Selain itu untuk role based access menggunakan file csv saja sehingga untuk custom role,
bisa dilakukan di dalam file csv yang ada di dalam repo ini ("rbac_policy.csv")

role yang ada pada saat ini adalah admin, member dan anonymous

admin memiliki akses yang tidak dibatasi.
member hanya memiliki akses tertentu.
sedangkan untuk user yang belum login akan masuk pada role anonymous

Untuk contoh hit api menggunakan postman bisa import dari file .postman_collection yang ada di dalam repo ini.
Mohon maaf masih berantakan dan contoh yang benar2 tested hanya yang ada di postman_collection.

Terima kasih.
